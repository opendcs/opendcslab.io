module.exports = {
 title: "OpenDCS Website",
 description: "Location for information related to OpenDCS",
 dest: "public",
 themeConfig: {
   sidebar: ['/' , '/SOON', '/ABOUT' ]
 }
}
