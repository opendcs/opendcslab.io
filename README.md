---
home: true
actionText: Soon... 👋
actionLink: /SOON
features:
- title: Components
  details: Data acquisition, feedback control, data logging
- title: Documentation
  details: All the things
footer: MIT Licensed | Copyright © 2017–2019 Geoff Johnson
---
